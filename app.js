var express = require('express');
/*<==== body-parser deals with the post request read the data and put them inside an object===>*/
var bodyParser = require('body-parser');
/*<==== import mongoDB to the application===>*/
var mongodb = require('mongodb');
var app = express();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

//We telling Express to use static file(html and images in this case)
app.use(express.static("views"));
app.use(express.static("images"));
app.use(express.static("css"));
//middlewares: expand express capabalilties to execute some tasks, 
//they get executed before the handlers(get, put).

//parse apps x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

//parse apps in json format
app.use(bodyParser.json());

//let db = [];
//Setting mongoDB parameters 
const mongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017';
var db;
var col;
//Connecting MongoDB to the client
mongoClient.connect(url, {userNewUrlParser: true}, function(err, client){
    if(err){
        console.log("Error: ", err);
    }else{
        console.log("Connected successfully to mongoDB server");
        db = client.db("fitTasks");
        col = db.collection("tasks");
    }
});

app.get('/', function(req, res){
    console.log('Thank you for the request');
    res.render('index.html');
});

app.get('/newtask', function(req, res){
    res.render('newtask.html');
});

app.get('/listtasks', function(req, res){
    //res.render('listtasks.html', {taskDB:db});
    col.find({}).toArray(function(err, data){
    res.render('listtasks.html', {taskDB:data});
    });
});

app.get('/updatetask', function(req, res){
    res.render("updatetask.html");
});

app.get('/deletetask', function(req, res){
    res.render("deletetask.html");
});

app.get('/deleteall', function(req, res){
    res.render("deleteall.html");
});
app.post('/newtask', function(req, res){
   /** var rec ={
        tName: req.body.taskname,
        tDue: req.body.taskdue,
        tDesc: req.body.taskdesc
    };
    db.push(rec);**/
    let tDetails = req.body;
    let newId = Math.round(Math.random()*1000); 
    col.insertOne({taskId: newId, taskName: tDetails.taskname, assignTo: tDetails.taskassign, taskDate:new Date(tDetails.taskdue), taskStat: tDetails.taskstat, taskDesc: tDetails.taskdesc});
    res.render('index.html');
});

app.post('/updatetask', function(req, res){
    let tDet = req.body;
    let filter = {taskId: parseInt(tDet.tid)};
    let update = {$set: {taskStat: tDet.newstat}};
    col.updateOne(filter, update);
    res.redirect("/listtasks");
});

app.post('/deletetask', function(req, res){
    let tId = req.body;
    let filter = {taskId: parseInt(tId.delid)};
    col.deleteOne(filter);
    res.redirect("/listtasks");
});

app.post('/deleteall', function(req, res){
    let tStat = req.body;
    let filter = {taskStat:tStat.delstat};
    col.deleteOne(filter);
    res.redirect("/listtasks");
});

app.listen(8080);